// 实现forEach(arr,cb) 函数，每次处理可能是同步的，也可能异步的； 下一步的处理必须在上一步完成之前

// function (){

// }

function forEach(arr, cb){
    // cb 运行可能是异步的
    // 每一个执行完 开始调用下一组 co 库的next实现 本质应该用递归回调来实现
    // 异步使用promise 的then 递归回调  Promise.resolve();
    let index = 0;
    const newCb = () => {
        cb(arr[index], index, arr);
        index ++;
        if( index < arr.length){
            newCb();
        }
    };
    newCb();
}

